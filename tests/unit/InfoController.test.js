import chai from 'chai';
import sinon from 'sinon';
import dirtyChai from 'dirty-chai';

import InfoController from '../../src/controller/InfoController';
import InfoService from '../../src/service/InfoService';

describe('Test errorHandler', () => {
  chai.use(dirtyChai);
  const { expect } = chai;
  const infoService = new InfoService();
  let infoController;
  let InfoServiceMock;

  beforeEach(() => {
    InfoServiceMock = sinon.mock(infoService);
    infoController = new InfoController({ infoService });
  });

  afterEach(() => {
    InfoServiceMock.restore();
  });

  describe('#getServerTime', () => {
    it('should calls status with 200 and json', (done) => {
      const res = {
        status: sinon.spy(() => res),
        json: sinon.spy(),
      };
      InfoServiceMock
        .expects('getServerTime')
        .once()
        .returns(Promise.resolve(new Date()));

      infoController
        .time(undefined, res)
        .then(() => {
          expect(res.status.calledOnce).to.be.true();
          expect(res.status.calledWith(200)).to.be.true();
          expect(res.json.calledOnce).to.be.true();
          done();
        })
        .catch(done);
    });

    it('should calls next with error', (done) => {
      const next = sinon.spy();
      const error = new TypeError();
      InfoServiceMock
        .expects('getServerTime')
        .once()
        .returns(Promise.reject(error));

      infoController
        .time(undefined, undefined, next)
        .then(() => {
          expect(next.calledOnce).to.be.true();
          expect(next.calledWith(error)).to.be.true();
          done();
        })
        .catch(done);
    });
  });
});
