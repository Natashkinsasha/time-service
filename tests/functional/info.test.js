import chai from 'chai';
import chaiHttp from 'chai-http';
import dirtyChai from 'dirty-chai';
import { SERVER_URL } from './util';

describe('Test info endpoint', () => {
  chai.use(chaiHttp);
  chai.use(dirtyChai);
  const { expect } = chai;

  describe('#GET /api/info/time', () => {
    it('should return server time', (done) => {
      chai
        .request(SERVER_URL)
        .get('/api/info/time')
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.have.property('body').to.be.a('string');
          done();
        })
        .catch(done);
    });
  });
});
