import chai from 'chai';
import request from 'supertest';
import dirtyChai from 'dirty-chai';

import App from '../../src/app/App';
import BasicRouter from '../../src/router/BasicRouter';
import InfoRouter from '../../src/router/InfoRouter';
import InfoController from '../../src/controller/InfoController';
import InfoService from '../../src/service/InfoService';


describe('Test InfoRouter', () => {
  chai.use(dirtyChai);
  const { expect } = chai;
  const infoService = new InfoService();
  const infoController = new InfoController({ infoService });
  const infoRouter = new InfoRouter({ infoController });
  const basicRouter = new BasicRouter({ infoRouter });
  const app = new App({ basicRouter });


  describe('#GET /api/info/time', () => {
    it('should return server time', () => request(app)
      .get('/api/info/time')
      .expect(200)
      .then((res) => {
        expect(res).to.have.property('body').to.be.a('string');
      }));
  });
});
