import http from 'http';
import config from 'node-config-env-value';

import logger from '../lib/logger';


export default class HttpServer {
  constructor({ app }) {
    this.server = http.createServer(app);
    this.port = config.get('PORT');
  }

    start = () => {
      this.server.listen(this.port, () => {
        logger.state(`Server start on ${this.port}`);
      });
    }
}
