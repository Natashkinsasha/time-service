import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';

import errorHandler from '../middleware/errorHandler';
import logger from '../middleware/logger';

export default class App {
  constructor({ basicRouter }) {
    const app = express();
    app.use(helmet());
    app.use(logger());
    app.use(cors());
    app.use(bodyParser.json());
    app.use('/api', basicRouter);
    app.use(errorHandler());
    return app;
  }
}
