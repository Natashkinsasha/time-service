import HttpServer from './app/HttpServer';
import App from './app/App';
import BasicRouter from './router/BasicRouter';
import InfoRouter from './router/InfoRouter';
import InfoController from './controller/InfoController';
import InfoService from './service/InfoService';
import logger from './lib/logger';

process.on('uncaughtException', (err) => {
  logger.error(err);
});

const infoService = new InfoService({});
const infoController = new InfoController({ infoService });
const infoRouter = new InfoRouter({ infoController });
const basicRouter = new BasicRouter({ infoRouter });
const app = new App({ basicRouter });
const httpServer = new HttpServer({ app });

httpServer.start();
