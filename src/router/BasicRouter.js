import { Router } from 'express';

export default class BasicRouter extends Router {
  constructor({ infoRouter }) {
    super();
    return this
      .use('/info', infoRouter);
  }
}
