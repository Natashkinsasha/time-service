import { Router } from 'express';

export default class InfoRouter extends Router {
  constructor({ infoController }) {
    super();
    return this
      .get('/time', infoController.time);
  }
}
