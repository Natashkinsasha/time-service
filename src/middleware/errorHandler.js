import Promise from 'bluebird';
import shortid from 'shortid';


export default () => (err, req, res, next) => { // eslint-disable-line no-unused-vars
  const failureId = shortid.generate();
  return Promise
    .reject(err)
    .catch(err => res.status(500).json({
      name: err.name,
      message: err.message,
      code: err.code,
      failureId,
    }));
};
